const gulp  = require('gulp');
const imagemin = require('gulp-imagemin');
const uglify  = require('gulp-uglify');
const sass = require('gulp-sass');
const concat  = require('gulp-concat');

// Top Level Functions
// - gulp.task: Define tasks
// - gulp.src: Point to files to use
// - gulp.dest: Point to the folder to output
// - gulp.watch: Watch folder and files for changes

gulp.task('message', function() {
    return console.log('Gulp is running');
});

// Copy all HTML files
gulp.task('copyHTML', function() {
    gulp.src('src/*.html')
        .pipe(gulp.dest('dist'));
});

// Optimize images
gulp.task('imageMin', () =>
	gulp.src('src/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
);

// Uglify JS
/*
gulp.task('minify', () =>
    gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
);
*/

// Compile SASS
gulp.task('sass', () =>
    gulp.src('src/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('dist/css'))
);

gulp.task('concatScripts', () =>
    gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
);

gulp.task('default', ['message', 'copyHTML', 'imageMin', 'sass', 'concatScripts']);

gulp.task('watch', function() {
    gulp.watch('src/js/*.js', ['concatScripts']);
    gulp.watch('src/src/iamges', ['imageMin']);
    gulp.watch('src/sass/*.scss', ['sass']);
    gulp.watch('src/*.html', ['copyHTML']);
});
